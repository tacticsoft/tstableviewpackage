# TSTableViewPackage #

A standalone Unity project repository for the [TSTableView](http://www.bitbucket.org/tacticsoft/tstableview) component. For more information, see  [TSTableView](http://www.bitbucket.org/tacticsoft/tstableview).

When cloning this repository, it is important to update submodules or else the actual code won't be included.